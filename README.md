## NO 1
Proses Transfer
1. tampilkan menu (1. trasnfer ke sesama ovo, 2. transfer ke rekening bank)
2. input pilihan_menu
3. switch(pilihan_menu)

    case 1 : 

    - input noHP_Tujuan
    - input nama_penerima
    - input jumlah_transfer
    - jika jumlah_trasnfer+admin lebih besar dari saldo, maka status pembayaran gagal
    - jika kurang dari sama dengan saldo, maka status pembayaran berhasil.
    - jika status pembayaran berhasil, cetak struk transaksi, dan masukan detail transaksi ke dalam array HistoriTransaksi, update saldo (saldo sekarang -= (jumlah_transaksi + biaya_admin))
    - jika status pembayaran gagal langsung kembali ke menu utama

    break

    case 2 :

    - input bankTujuan
    - input nama_penerima
    - input nomor rekening
    - input jumlah transfer
    - jika jumlah_trasnfer+admin lebih besar dari saldo, maka status pembayaran gagal
    - jika kurang dari sama dengan saldo, maka status pembayaran berhasil.
    - jika status pembayaran berhasil, cetak struk transaksi, dan masukan detail transaksi ke dalam array HistoriTransaksi, update saldo (saldo sekarang -= (jumlah_transaksi + biaya_admin))
    - jika status pembayaran gagal langsung kembali ke menu utama

    break


        public void transfer() {
            clearScreen();
            User user = getUserbyNoHP(noHP);
            Transfer transfer = new Transfer(user);
            boolean status_bayar = false;
            int tujuan;
            int minimal_transfer = 10000;
            int jumlahTransfer = 0;
            int biayaAdmin = 0;
            int totalTransaksi;
            String namaBank;
            String jenisTransaksi = "Transfer";
            String no_referensi = "";
            String tanggal_transaksi = "";
            String waktu_transaksi = "";
            String metode = "";
            String noTujuan = "";
            String namaTujuan = "";

            System.out.println("==================================================");
            System.out.println("\n                  Transfer");
            System.out.println("\n==================================================");
            System.out.println(" 1. | Ke Sesama OVO");
            System.out.println("--------------------------------------------------");
            System.out.println(" 2. | Ke Rekening Bank");
            System.out.println("--------------------------------------------------");
            System.out.print(" Input Pilihan\t: ");
            tujuan = input.nextInt();
            input.nextLine();

            switch (tujuan) {
                case 1:
                    clearScreen();
                    metode = "Transfer Sesama OVO";
                    String atasNama;
                    biayaAdmin = 0;

                    System.out.println("==================================================");
                    System.out.println("\n            Transfer ke Sesama OVO");
                    System.out.println("\n             Minimal Transfer = " + minimal_transfer);
                    System.out.println("\n==================================================");
                    System.out.print(" \n Masukkan nomor HP penerima\t: ");
                    noTujuan = input.nextLine();
                    transfer.setNomorPenerima(noTujuan);
                    System.out.print(" \n Masukkan nama penerima\t\t: ");
                    namaTujuan = input.nextLine();
                    transfer.setNamaPenerima(namaTujuan);
                    transfer.setJenisTransaksi(jenisTransaksi);

                    do {
                        System.out
                                .print(" \n Masukkan jumlah transfer\t: ");
                        jumlahTransfer = input.nextInt();
                        transfer.setgetjumlah_transaksi(jumlahTransfer);
                        input.nextLine();

                        if (jumlahTransfer < minimal_transfer) {
                            System.out.println("\n Nominal Transfer kurang dari Nilai Minimal");
                            System.out.println("\n--------------------------------------------------");

                        }
                    } while (jumlahTransfer < minimal_transfer);

                    totalTransaksi = jumlahTransfer + biayaAdmin;

                    if (totalTransaksi <= user.getBalance()) {
                        no_referensi = transfer.makeReferensi();
                        transfer.setReferensi(no_referensi);
                        tanggal_transaksi = transfer.makeTanggal();
                        transfer.setTanggal(tanggal_transaksi);
                        waktu_transaksi = transfer.makeWaktu();
                        transfer.setWaktu(waktu_transaksi);
                        transfer.updateSaldo(totalTransaksi);
                        status_bayar = true;
                        transfer.getStatusBayar(status_bayar);
                        clearScreen();
                        transfer.cetakStruk();

                    } else {
                        System.out.println("\n Saldo Anda tidak cukup");
                        menuUtama();
                    }
                    break;

                case 2:
                    clearScreen();
                    biayaAdmin = 2500;
                    metode = "Transfer ke Rekening Bank";
                    transfer.setgetBiayaAdmin(biayaAdmin);
                    System.out.println("==================================================");
                    System.out.println("\n            Transfer ke Rekening Bank");
                    System.out.println("\n             Minimal Transfer = " + minimal_transfer);
                    System.out.println("\n==================================================");
                    System.out.println(" BANK TUJUAN :");
                    System.out.println(" BCA\t\tBRI\t\tMANDIRI");
                    System.out.println(" CIMB NIAGA\tBNI\t\tBTPN");
                    System.out.print(" \n Pilih Bank Tujuan\t: ");
                    namaBank = input.nextLine();
                    transfer.setJenisTransaksi(namaBank.toUpperCase());
                    System.out.print(" Input Nama Penerima\t: ");
                    namaTujuan = input.nextLine();
                    transfer.setNamaPenerima(namaTujuan);
                    System.out.print(" Input Nomor Rekening\t: ");
                    noTujuan = input.nextLine();
                    transfer.setNomorPenerima(noTujuan);

                    do {
                        System.out
                                .print(" \n Masukkan jumlah transfer\t: ");
                        jumlahTransfer = input.nextInt();
                        transfer.setgetjumlah_transaksi(jumlahTransfer);
                        input.nextLine();

                        if (jumlahTransfer < minimal_transfer) {
                            System.out.println("\n Nominal Transfer kurang dari Nilai Minimal");
                            System.out.println("\n--------------------------------------------------");

                        }
                    } while (jumlahTransfer < minimal_transfer);

                    totalTransaksi = jumlahTransfer + biayaAdmin;

                    if (totalTransaksi <= user.getBalance()) {
                        no_referensi = transfer.makeReferensi();
                        transfer.setReferensi(no_referensi);
                        tanggal_transaksi = transfer.makeTanggal();
                        transfer.setTanggal(tanggal_transaksi);
                        waktu_transaksi = transfer.makeWaktu();
                        transfer.setWaktu(waktu_transaksi);
                        transfer.updateSaldo(totalTransaksi);
                        status_bayar = true;
                        transfer.getStatusBayar(status_bayar);
                        clearScreen();
                        transfer.cetakStruk();
                    } else {
                        System.out.println("\n Saldo Anda tidak cukup");
                        menuUtama();
                    }
                    break;
            }
            histori = new HistoriTransaksi(metode, user, noTujuan, namaTujuan, jenisTransaksi, no_referensi,
                    tanggal_transaksi, waktu_transaksi, jumlahTransfer, biayaAdmin);
            riwayat.add(histori);
            System.out.print("Klik ENTER untuk kembali ke menu utama ");
            input.nextLine();
            menuUtama();
    }

## NO 2

PENJELASAN ALgoritma NO 1

Transkasi Tranfer memiliki 2 metode, yaitu Transfer ke sesama OVO dan transfer ke rekening bank. Pada pemilihan kedua program itu, kita diminta untuk melakukan input menu (1 untuk transfer ke sesama ovo, dan 2 untuk transfer ke rekening bank), disini saya menggunakan switch case.

dari kedua pilihan itu untuk logikanya sebenarnya sama saja. di awal diminta info dari tujuan transfer kita, lalu jumlah transaksi. biaya admin untuk menu 1 adalah 0, dan untuk menu 2 adalah 2500.

setelah itu akan dilakukan pengecekan, dengan menghitung total transaksi dulu. total transaksi adalah jumlah transaksi ditambah biaya admin.

jika total melebihi saldo, maka transkasi gagal dan akan menampilkan pemberitahuan bahwa transaksi tidak berhasil, lalu akan diarahkan untuk kembali ke menu utama.

jika total kurang atau bahkan sama dengan salo, maka transaksi bisa dilakukan (berhasil). ketika transaksi berhasil, akan ditampilkan bukti transaksi. lalu dilakukan update saldo. yaitu pengurangan saldo awal oleh total transaksi. setelah pengurangan saldo, detail dari transaksi itu akan dimasukan ke dalam array Histori Transaksi, lalu akan diarahkan kembali ke menu utama.

## NO 3
KONSEP DASAR Object Oriented Programming(OOP)
1. Abstraction

Abstraksi adalah kemampuan menyembunyikan detail implementasi yang kompleks dan hanya menampilkan bagian yang inti(penting) saja. Dalam hal ini abstraksi membantu kita dalam untuk memodelkan sebuah objek dengan lebih sederhana. Abstraksi bisa dicapai melalui kelas abstrak atau penggunaan implementasi.

Pada projek implementasi OVO yang saya buat, penerapan abstraksi terdapat pada kelas Transaksi, yang berperan sebagai superclass dari kels TopUp, Transfer, dan TarikTunai.

2. Encaptulation

Encaptulation atau bisa disebut dengan istilah pengkapsulan. pada enkapsulasi, data dari suatu class hanya bisa diakses melalui method-method tertentu yang telah ditentukan sebelumnya. 

Pada enkapsulasi ini dikenal juga istilah access modifier yang berguna untuk mengatur aksesbilitas dari data-data yang terdapat dari suatu class. Ada beberapa access modifier yang basa digunakan pada enkapsulasi, yaitu :
- public, data dapat diakses dari mana saja.
- private, data hanya bisa diakses oleh metode-metode lain dari kelas yang sama.
- protected, data hanya bisa diakses oleh kelas yang sama dan kelas turunannya.
- default, data hanya bisa diakses oleh seluruh kelas yang berada pada package yang sama.

enkapsulasi dapat membantu kita untuk menerapkan abstraksi, juga untuk meningkatkan keamanan.

3. Inheritance

inheritance dikenal juga dengan konsep penurunan. dimana supperclass bisa menurunkan atribut dan methodnya pada subclass. dengan menggunakan konsep inheritance ini kita bisa memanfaatkan reusabilitas dan menghindari duplikasi kode. pada inheritance juga bisa dilakukan modifikasi, maksudnya.. method dari superclass bisa dimodifikasi lagi oleh subclass nya.

pada program implementasi OVO yang saya buat, inheritance diterapkan pada class Transaksi sebagai superclass, dan TopUP, Transfer, dan Tarik Tunai sebagai subclass nya.

4. Polymorphism

Polymorphism adalah saat ketika bebrapa objek yang berbeda memiliki method dengan nama yang sama. methodnya sama, perilakunya berbeda.

pada program implementasi OVO, polymorphism terdapat pada method cetakstruk().




## NO 4

Ini adalah salah satu potongan kode yang menerapkan prinsip enkapsulasi.  

Jadi pada kode dibawah ini bisa kita lihat bahwa atributnya menggunakan access modifier private, yang mana berarti atribut-atribut tersebut hanya bisa diakses oleh method-method yang berada dalam class User saja.

Lalu, metode pengkapsulan terdapat pada method getPhoneNumber(), getPassword(), getName(), getBalance(). method tersebut dimaksudkan agar nilai phoneNumber, password, balance, dan nama hanya bisa diambil melalui method tersebut. begitu juga dengan method setPassword() dan setBalance() yang artinya password dan balance hanya bisa di ganti melalui method tersebut, diluar itu tidak bisa.
    

    classUser{
        private String phoneNumber;
        private String password;
        private String name;
        private int balance = 0;

        public User(String phoneNumber, String password, String name, int balance) {
            this.phoneNumber = phoneNumber;
            this.password = password;
            this.name = name;
            this.balance = balance;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public String getPassword() {
            return password;
        }

        public String getName() {
            return name;
        }

        public int getBalance() {
            return balance;
        }

        public void setBalance(int balance) {
            this.balance = balance;
        }

        public void setPassword(String Password) {
            this.password = Password;
        }

    }


## NO 5

Kelas yang menerapkan konsep abstraksi adalah kelas transaksi.

pada awal pembuatan class, untuk menyatakan class Transaksi adalah class abstrak, diberikan kata kunci "abstract". Di dalam kelas tersebut terdapat method abstrak juga method konkret.

method abstractnya adalah **cetakStruk()**, dan method yang lainnya merupakan method konkret.

karena class transaksi merupakan class abstrak, maka tidak dapat diinstalisasi menjadi objek secara langsung, namun disini class Transkasi dapat berperan menjadi class induk yang akan mewariskan atribut juga methodnya kepada subclassnya.

        abstract class Transaksi{
            Scanner input = new Scanner(System.in);
            protected int biaya_admin;
            protected int jumlah_transaksi;
            protected String no_referensi;
            protected String tanggal_transaksi;
            protected String waktu_transaksi;
            protected String noTujuan;
            protected String namaTujuan;
            protected String jenisTransaksi;
            protected String status_bayar;
            protected String metode;
            protected ovo objekOvo;
            protected User user;

            public abstract void cetakStruk();

            public Transaksi() {
                this.metode = metode;
                this.user = user;
                this.noTujuan = noTujuan;
                this.namaTujuan = namaTujuan;
                this.jenisTransaksi = jenisTransaksi;
                this.jumlah_transaksi = jumlah_transaksi;
                this.biaya_admin = biaya_admin;
                this.no_referensi = no_referensi;
                this.tanggal_transaksi = tanggal_transaksi;
                this.waktu_transaksi = waktu_transaksi;
            }

            public String getTanggal() {
                return tanggal_transaksi;
            }

            public void setTanggal(String tanggal) {
                this.tanggal_transaksi = tanggal_transaksi;
            }

            public String makeTanggal() {
                // Mendapatkan tanggal sekarang
                Date tanggalSekarang = new Date();

                // Mengatur format tanggal yang diinginkan
                DateFormat formatTanggal = new SimpleDateFormat("dd/MM/yyyy");

                // Mengonversi tanggal menjadi string sesuai format
                String tanggalString = formatTanggal.format(tanggalSekarang);
                return tanggalString;
            }

            public String getReferensi() {
                return no_referensi;
            }

            public String makeReferensi() {
                String karakter = "abcdefghijklmnopqrstuvwxyz1234567890";
                int panjang = 20;
                Random random = new Random();
                StringBuilder sb = new StringBuilder();
                String ref;

                for (int i = 0; i < panjang; i++) {
                    int index = random.nextInt(karakter.length());
                    char randomChar = karakter.charAt(index);
                    sb.append(randomChar);
                }

                return sb.toString();
            }

            public void setReferensi(String no_referensi) {
                this.no_referensi = no_referensi;
            }

            public int getkodeTransaksi() {
                int kodetransaksi;
                Random kode = new Random();
                kodetransaksi = kode.nextInt(6);
                return kodetransaksi;
            }

            public String makeWaktu() {
                // Mendapatkan waktu sekarang
                LocalTime waktuSekarang = LocalTime.now();

                // Mengatur format waktu yang diinginkan
                DateTimeFormatter formatWaktu = DateTimeFormatter.ofPattern("HH:mm:ss");

                // Mengonversi waktu menjadi string sesuai format
                String waktuString = waktuSekarang.format(formatWaktu);
                return waktuString;
            }

            public void setWaktu(String waktu) {
                this.waktu_transaksi = waktu;
            }

            public String getWaktu() {
                return waktu_transaksi;
            }

            public int getJumlahTransaksi() {
                return jumlah_transaksi;
            }

            public User getUser() {
                return user;
            }

            public int getBiayaAdmin() {
                return biaya_admin;
            }

            public String getNamaPengirim() {
                return user.getName();
            }

            public String getNoPengirim() {
                return user.getPhoneNumber();
            }

            public void setNamaPenerima(String nama) {
                this.namaTujuan = nama;
            }

            public void setNomorPenerima(String nomor) {
                this.noTujuan = nomor;
            }

            public String getNamaTujuan() {
                return namaTujuan;
            }

            public String getNoTujuan() {
                return noTujuan;
            }

            public void setJenisTransaksi(String jenis) {
                this.jenisTransaksi = jenis;
            }

            public String getJenisTrnsaksi() {
                return jenisTransaksi;
            }

            public String getMetode() {
                return metode;
            }

            public String getJenisTransaksi() {
                return jenisTransaksi;
            }
        }
    }



## NO 6
Konsep inheritance dan polymorphism yang akan saya lampirkan masih terkait dengan no sebelumnya, karena masih diterapkan di class Transaksi(sebagai superclass), dan TopUp, Transfer, TarikTunai (sebagai subclass). Polymorphism yang diterapkan terdapat pada method cetakStruk().

polymorphism itu kan ketika objek yang berbeda menggunakan method dengan nama yg sama ya..

[SS objek "up" pada method topUp di class main yg menggunakan method cetakStruk()](https://gitlab.com/danggita000/uts-oop/-/issues/1/designs/cetakStruk_topup.png)
[SS objek "transfer" pada method Transfer di class main yg menggunakan method cetakStruk()](https://gitlab.com/danggita000/uts-oop/-/issues/1/designs/cetakStruk_transfer.png)



Transaksi sebagai superclass:

        abstract class Transaksi {
            Scanner input = new Scanner(System.in);
            protected int biaya_admin;
            protected int jumlah_transaksi;
            protected String no_referensi;
            protected String tanggal_transaksi;
            protected String waktu_transaksi;
            protected String noTujuan;
            protected String namaTujuan;
            protected String jenisTransaksi;
            protected String status_bayar;
            protected String metode;
            protected ovo objekOvo;
            protected User user;

            public abstract void cetakStruk();

            public Transaksi() {
                this.metode = metode;
                this.user = user;
                this.noTujuan = noTujuan;
                this.namaTujuan = namaTujuan;
                this.jenisTransaksi = jenisTransaksi;
                this.jumlah_transaksi = jumlah_transaksi;
                this.biaya_admin = biaya_admin;
                this.no_referensi = no_referensi;
                this.tanggal_transaksi = tanggal_transaksi;
                this.waktu_transaksi = waktu_transaksi;
            }

            public String getTanggal() {
                return tanggal_transaksi;
            }

            public void setTanggal(String tanggal) {
                this.tanggal_transaksi = tanggal_transaksi;
            }

            public String makeTanggal() {
                // Mendapatkan tanggal sekarang
                Date tanggalSekarang = new Date();

                // Mengatur format tanggal yang diinginkan
                DateFormat formatTanggal = new SimpleDateFormat("dd/MM/yyyy");

                // Mengonversi tanggal menjadi string sesuai format
                String tanggalString = formatTanggal.format(tanggalSekarang);
                return tanggalString;
            }

            public String getReferensi() {
                return no_referensi;
            }

            public String makeReferensi() {
                String karakter = "abcdefghijklmnopqrstuvwxyz1234567890";
                int panjang = 20;
                Random random = new Random();
                StringBuilder sb = new StringBuilder();
                String ref;

                for (int i = 0; i < panjang; i++) {
                    int index = random.nextInt(karakter.length());
                    char randomChar = karakter.charAt(index);
                    sb.append(randomChar);
                }

                return sb.toString();
            }

            public void setReferensi(String no_referensi) {
                this.no_referensi = no_referensi;
            }

            public int getkodeTransaksi() {
                int kodetransaksi;
                Random kode = new Random();
                kodetransaksi = kode.nextInt(6);
                return kodetransaksi;
            }

            public String makeWaktu() {
                // Mendapatkan waktu sekarang
                LocalTime waktuSekarang = LocalTime.now();

                // Mengatur format waktu yang diinginkan
                DateTimeFormatter formatWaktu = DateTimeFormatter.ofPattern("HH:mm:ss");

                // Mengonversi waktu menjadi string sesuai format
                String waktuString = waktuSekarang.format(formatWaktu);
                return waktuString;
            }

            public void setWaktu(String waktu) {
                this.waktu_transaksi = waktu;
            }

            public String getWaktu() {
                return waktu_transaksi;
            }

            public int getJumlahTransaksi() {
                return jumlah_transaksi;
            }

            public User getUser() {
                return user;
            }

            public int getBiayaAdmin() {
                return biaya_admin;
            }

            public String getNamaPengirim() {
                return user.getName();
            }

            public String getNoPengirim() {
                return user.getPhoneNumber();
            }

            public void setNamaPenerima(String nama) {
                this.namaTujuan = nama;
            }

            public void setNomorPenerima(String nomor) {
                this.noTujuan = nomor;
            }

            public String getNamaTujuan() {
                return namaTujuan;
            }

            public String getNoTujuan() {
                return noTujuan;
            }

            public void setJenisTransaksi(String jenis) {
                this.jenisTransaksi = jenis;
            }

            public String getJenisTrnsaksi() {
                return jenisTransaksi;
            }

            public String getMetode() {
                return metode;
            }

            public String getJenisTransaksi() {
                return jenisTransaksi;
            }
        }


Transfer sebagai subclass :

        class Transfer extends Transaksi {
            private ArrayList<Transaksi> historiTransaksi;
            private String jenisTransaksi = "Transfer";

            public Transfer(User user) {
                super();
                this.user = user;
            }

            public int setgetBiayaAdmin(int biaya_admin) {
                this.biaya_admin = biaya_admin;
                return biaya_admin;
            }

            public int setgetjumlah_transaksi(int jumlah_transaksi) {
                this.jumlah_transaksi = jumlah_transaksi;
                return jumlah_transaksi;
            }

            public int updateSaldo(int totalTransaksi) {
                user.setBalance(user.getBalance() - totalTransaksi);
                return user.getBalance();
            }

            public String getStatusBayar(boolean status_bayar) {
                if (status_bayar) {
                    this.status_bayar = "Berhasil";
                }
                return this.status_bayar;
            }

            public void cetakStruk() {
                System.out.println("==================================================\n");
                System.out.println("                       OVO");
                System.out.println("\n--------------------------------------------------");
                System.out.println(" Status Transaksi\t: " + status_bayar);
                no_referensi = getReferensi();
                System.out.println(" No. Referensi\t\t: " + no_referensi);
                tanggal_transaksi = getTanggal();
                System.out.println("\n Tanggal Transaksi\t: " + tanggal_transaksi);
                waktu_transaksi = getWaktu();
                System.out.println(" Waktu Transaksi\t: " + waktu_transaksi);
                System.out.println("\n--------------------------------------------------");
                System.out.println("\n Dari\t\t\t: " + getNamaPengirim());
                System.out.println(" OVO\t\t\t: " + getNoPengirim());
                namaTujuan = getNamaTujuan();
                System.out.println("\n Penerima\t\t: " + namaTujuan);
                noTujuan = getNoTujuan();
                System.out.println(" " + getMetode() + "\t\t\t: " + noTujuan);
                System.out.println("\n--------------------------------------------------");
                System.out.println(" Detail Pembayaran ");
                System.out.println("--------------------------------------------------");
                jumlah_transaksi = setgetjumlah_transaksi(jumlah_transaksi);
                System.out.println("\n Nominal\t\t: " + jumlah_transaksi);
                biaya_admin = setgetBiayaAdmin(biaya_admin);
                System.out.println(" Biaya\t\t\t: " + biaya_admin);
                System.out
                        .println(" Total\t\t\t: " + (setgetBiayaAdmin(biaya_admin + setgetjumlah_transaksi(jumlah_transaksi))));
                System.out.println("\n--------------------------------------------------");
            }
        }

class TopUp secagai subclass :

        class TopUp extends Transaksi {
            private ArrayList<Transaksi> historiTransaksi;
            private String jenisTransaksi = "Top Up";

            public TopUp(User user) {
                super();
                this.user = user;
            }

            public String setgetMetode(String metode) {
                this.metode = metode;
                return metode;
            }

            public String getNoTujuan() {
                return user.getPhoneNumber();
            }

            public int setgetJumlahTransaksi(int jumlah_transaksi) {
                this.jumlah_transaksi = jumlah_transaksi;
                return jumlah_transaksi;
            }

            public int setgetBiayaAdmin(int biaya_admin) {
                this.biaya_admin = biaya_admin;
                return biaya_admin;
            }

            public int updateSaldo(int totalTransaksi) {
                user.setBalance(user.getBalance() + totalTransaksi);
                return user.getBalance();
            }

            public void cetakMenu() {
                System.out.println("\n Pilih Metode :");
                System.out.println("\n==================================================");
                System.out.println(" 1. | Alfamart");
                System.out.println("--------------------------------------------------");
                System.out.println(" 2. | Indomaret");
                System.out.println("--------------------------------------------------");
                System.out.println(" 3. | ATM");
                System.out.println("--------------------------------------------------");
                System.out.println(" 4. | Internet / Mobile Banking");
                System.out.println("--------------------------------------------------");
                System.out.println(" 5. | Pengemudi Grab");
                System.out.println("--------------------------------------------------");
                System.out.println(" 6. | Tokopedia");
                System.out.println("--------------------------------------------------");
            }

            public void detailTransaksi(String metode, int pilihan, int totalTransaksi, int biaya_admin) {
                System.out.println("--------------------------------------------------");
                System.out.println(" Detail Transaksi ");
                System.out.println("--------------------------------------------------");
                System.out.println(" Metode Transaksi\t\t\t: " + metode);
                System.out.println(" Nominal Top Up\t\t\t\t: " + totalTransaksi);
                System.out.println(" Biaya Top Up\t\t\t\t: " + biaya_admin);
                System.out.println("--------------------------------------------------");
                System.out.println(" Total Pembayaran\t\t\t: " + (totalTransaksi + biaya_admin));
                this.jumlah_transaksi = totalTransaksi;
                updateSaldo(jumlah_transaksi);
            }

            public void menuBank() {
                System.out.println("==================================================");
                System.out.println(" Pilih Bank : ");
                System.out.println("--------------------------------------------------");
                System.out.println(" BCA");
                System.out.println("--------------------------------------------------");
                System.out.println(" MANDIRI");
                System.out.println("--------------------------------------------------");
                System.out.println(" BRI");
                System.out.println("--------------------------------------------------");
                System.out.println(" BNI");
                System.out.println("--------------------------------------------------");
                System.out.println(" BJB");
                System.out.println("--------------------------------------------------");
                System.out.println(" BJB SYARIAH");
                System.out.println("--------------------------------------------------");
                System.out.println(" BANK LAINNYA");
                System.out.println("--------------------------------------------------");
            }

            public void cetakStruk() {
                System.out.println("==================================================\n");
                System.out.println("                       OVO");
                System.out.println("\n--------------------------------------------------");
                System.out.println(" Status Transaksi\t: " + status_bayar);
                no_referensi = getReferensi();
                System.out.println(" No. Referensi\t\t: " + no_referensi);
                tanggal_transaksi = getTanggal();
                System.out.println("\n Tanggal Transaksi\t: " + tanggal_transaksi);
                waktu_transaksi = getWaktu();
                System.out.println(" Waktu Transaksi\t: " + waktu_transaksi);
                System.out.println("\n--------------------------------------------------");
                metode = setgetMetode(metode);
                System.out.println("\n Metode\t\t\t: " + metode);
                noTujuan = getNoTujuan();
                System.out.println(" No. Tujuan\t\t: " + noTujuan);
                System.out.println("\n--------------------------------------------------");
                System.out.println(" Detail Pembayaran ");
                System.out.println("--------------------------------------------------");
                jumlah_transaksi = setgetJumlahTransaksi(jumlah_transaksi);
                System.out.println("\n Nominal\t\t: " + jumlah_transaksi);
                biaya_admin = setgetBiayaAdmin(biaya_admin);
                System.out.println(" Biaya\t\t\t: " + biaya_admin);
                System.out
                        .println(" Total\t\t\t: " + (setgetBiayaAdmin(biaya_admin + setgetJumlahTransaksi(jumlah_transaksi))));
                System.out.println("\n--------------------------------------------------");
                System.out.println(" Klik ENTER untuk kembali ke menu utama : ");
            }
        }

class Tarik tunai sebagai subclass :

        class tarikTunai extends Transaksi {
            public tarikTunai(User user) {
                super();
                this.user = user;

            }

            public int setgetBiayaAdmin(int biaya) {
                this.biaya_admin = biaya;
                return biaya_admin;
            }

            public int updateSaldo(int jumlah_transaksi, int biaya_admin) {
                user.setBalance(user.getBalance() - (jumlah_transaksi + biaya_admin));
                return user.getBalance();
            }

            public boolean getStatus(int jumlah_transaksi, int biaya_admin) {
                if ((user.getBalance() - (jumlah_transaksi + biaya_admin)) >= 0) {
                    return true;
                }
                return false;
            }

            public void konfirmasi(String metode, int jumlah_transaksi, int biaya_admin) {
                char konfir;
                this.metode = metode;
                this.jumlah_transaksi = jumlah_transaksi;
                this.biaya_admin = biaya_admin;

                System.out.println("==================================================");
                System.out.println("\n KONFIRMASI TARIK TUNAI");
                System.out.println("\n--------------------------------------------------");
                System.out.println("\n No. HP\t\t: 20258-" + user.getPhoneNumber());
                System.out.println("\n Kode Transaksi\t: " + getkodeTransaksi());
                System.out.println("\n--------------------------------------------------");
                System.out.println("\n Metode Penarikan\t: " + metode);
                System.out.println("\n Jumlah Penarikan\t: " + jumlah_transaksi);
                System.out.println("\n Biaya Admin\t: " + biaya_admin);
                System.out.println("\n--------------------------------------------------");
                System.out.println(" Total Pembayaran\t: " + (jumlah_transaksi + biaya_admin));
                System.out.println("--------------------------------------------------");
            }

            public void cetakStruk() {
                System.out.println(" ");
            }

        }


## NO 7
Karena kemungkinan saya salah tangkap dengan soalnya... agak kurang paham.. tapi disini saya akan mencoba menidentifikasi OOP pada usecase yang saya buat.

1. Objek
Hal yang paling khas pada OOP adalah keberadaan Objek.
Menurut saya objek pokok pada aplikasi OVO adalah User, Admin, dan Transaksi.

2. Atribut
Pada OOP, setiap objek pasti memiliki atribut.
    - Atribut pada User : nama, noHP, password, dan saldo
    - Atribut pada Admin : nama, no Identitas(yang dimiliki untuk menandakan bahwa dirinya adalah admin), dan password
    - Atribut pada Transaksi : jumlah transaksi, biaya admin, tanggal dan waktu transaksi, identitas aktor transaksi(pengirim dan/atau penerima), juga kode unik transaksi yang berguna untuk melacak/ mengecek transaksi tersebut.

3. Method atau perilaku

    Disini method mengacu pada "apa yang bisa dilakukan oleh objek"

    USER :

    - Menurut saya inti dari aplikasi ini ada pada objek User dengan method transaksi. "User bisa melakukan Transaksi" sebagai method utama dari aplikasi ini. namun utnuk bisa melakukan transaksi, User harus melalui beberapa tahapan dahulu. tahapan tersebut juga berupa method yakni, User bisa Masuk ke dalam aplikasi. "Masuk" disini jika mengacu pada usecase tentunya harus telah melakukan daftar/registrasi.

    - Karena transaksi memiliki beberapa jenis, mengakibatkan user harus dapat memilih transaksi apa yang akan dia lakukan. 

    - User juga memiliki kemampuan untuk mengubah atau mengupdate akunnya, seperti mengubah nama, mengubah no telepon dan passwordnya.

    - untuk meningkatkan kepuasan pengguna, user harus bisa menyampaikan keluhannya terhadap aplikasi. keluhan tersebut disampaikan pkepada admin.

    - karena adanya fitur promo yang disediakan oleh OVO, user harus bisa menggunakan fitur promo tersebut.

    ADMIN :

    - untuk mengawasi jalannya aplikasi, admin bisa mengecek transaksi apa saja yang dilakukan user.

    - admin juga bisa mengecek daftar user, dan termasuk informasi yang dimiliki oleh tiap user

    - admin bisa memantau jalannya aplikasi untuk menganalisis data dan perilaku user, ini berguna untuk meningkatkan performa aplikasi nantinya.

    - Karena pada aplikasi OVO terdapat berbagai macam promo, tentunya admin harus terus mengupdate mengenai informasi promo tersebut. 

    TRANSAKSI :

    - disini transaksi harus bisa menampilkan waktu dan tanggal ketika dilakukannya transaksi.

    - tiap transaksi harus dipastikan meiliki kode unik

    - ketika transaksi berahsil, objek transaksi menampilkan bukti atau struk transaksi.

    - karena memiliki berbagai jenis transkasi, harus diberikan pilihan bagi user untuk bisa menentukan transaksi jenis mana yang akan digunakan.

4. Abstraksi, dan Inheritance

    Di implementasi aplikasi OVO ini penggunaan abstraksi, dibarengi dengan penerapan inheritance.

    Disini penggunaan abstraksi digunakan untuk memperingkas kode, sehingga mempermudah memahami hubungan antar obejknya. nah karena kodenya lebih ringkas, mudah juga untuk melakukan perbaikan atau peningkatan. 

    sedangkan konsep inheritance digunakan untuk meningkatkan fleksibilitas dan penerapan reusability. diketahui bahwa user dapat melakukan berbagai jenis transaksi, top up, tranfer, tarik tunai, pembelian pulsa/paket data, dan transaksi-transaksi lainnya. Untuk mempermudah dan menghindari penulisan kode ganda, penggunaan konsep inheritance sangat berguna. karena disini ransfer memiliki berbagai jenis, sehingga dijadikanlah transfer sebagia class induk, dan jenis-jenisnya sebagai class anak

5. enkapsulasi

    Dalam aplikasi OVO, yang mengutamakan keamnanan, mpenerapan enkapsulasi atau pengkapsulan sangat penting. Agar akses kepada data tertentu dapat dibatasi, sehingga lebih aman. 

    OVO menyimpan informasi pengguna seperti nama, alamat, dan informasi keuangan,  OVO memproses transaksi keuangan seperti pembayaran dan pengiriman dana, dan OVO juga  mungkin terhubung dengan layanan pihak ketiga seperti bank atau penyedia layanan pembayaran. Enkapsulasi memungkinkan implementasi ketiga hal tersebut disembunyikan dari pihak eksternal.

## NO 8
TABEL USE CASE
| actor | use case | prioritas | Status |
| ------ | ------ | ------ | ------ |
public | melakukan registrasi | 100 | ✅
user | melakukan login | 100 | ✅
user | mengganti password | 90 | ✅
user | melakukan top up| 100 | ✅
user | melakukan transfer |100 | ✅
user | melakukan tarik tunai | 100 | ✅
user | memilih metode transaksi(top up, transfer, tarik tunai) | 100 | ✅
user | menentukan nominal transaksi(top up, transfer, tarik tunai, dll)  | 100 | ✅
user | melakukan pembelian pulsa | 80 | ❌
user | melakukan pembelian paket data | 80 | ❌
user | melakukan pembayaran PLN | 80 | ❌
user | melakukan pencarian pajak PBB | 80 | ❌
user | melakukan pembayaran pajak PBB | 80 | ❌
user | melakukan perubahan profil | 90 | ❌
user | melakukan logout dari akun |90 | ❌
user | mengecek histori transaksi |90 | ✅
user | melaporkan masalah yang terjadi | 90 | ❌
user | menggunakana promo yang ditawarkan OVO | 80 | ❌
admin | menghapus user | 80 | ❌
admin | mengecek info user | 90 | ❌
admin | mengecek daftar user | 90 | ❌
admin | update promo | 70 |❌
admin | menerima laporan | 90 | ❌
admin | mengirim timbal balik atau respon dari laporan pengguna | 90 | ❌
admin | menganalisis data pengguna | 90 |❌
admin | mengakses riwayat transaksi pengguna | 90 |❌


[DIAGRAM CLASS](https://gitlab.com/danggita000/uts-oop/-/issues/2/designs/Diagram_Tanpa_Judul.drawio__18_.png)

## NO 9
video youtube 

[part1](https://www.youtube.com/watch?v=Lt10GdFC1hY)

[part2](https://www.youtube.com/watch?v=6SJ6MXSISx4)

## NO 10
[SCREENSHOT](https://gitlab.com/danggita000/uts-oop/-/issues/3/)
