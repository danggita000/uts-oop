import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.transform.Source;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

class Admin {
    private String noHP;
    private String password;
    private String name;

    public Admin(String noHP, String password, String name) {
        this.noHP = noHP;
        this.password = password;
        this.name = name;
    }
}

class userManagemen {
    ArrayList<User> users;

    public userManagemen(ArrayList<User> users) {
        this.users = users;
    }

    public void removeUser() {

    }

    public ArrayList<User> getAllUsers() {
        return User;
    }
}

class User {
    private String phoneNumber;
    private String password;
    private String name;
    private int balance = 0;

    public User(String phoneNumber, String password, String name, int balance) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.name = name;
        this.balance = balance;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setPassword(String Password) {
        this.password = Password;
    }
}

abstract class Transaksi {
    Scanner input = new Scanner(System.in);
    protected int biaya_admin;
    protected int jumlah_transaksi;
    protected String no_referensi;
    protected String tanggal_transaksi;
    protected String waktu_transaksi;
    protected String noTujuan;
    protected String namaTujuan;
    protected String jenisTransaksi;
    protected String status_bayar;
    protected String metode;
    protected ovo objekOvo;
    protected User user;

    Transaksi() {
        this.jumlah_transaksi = 0;
        this.no_referensi = getReferensi();
        this.tanggal_transaksi = getTanggal();
        this.waktu_transaksi = getWaktu();
    }

    public String getTanggal() {
        // Mendapatkan tanggal sekarang
        Date tanggalSekarang = new Date();

        // Mengatur format tanggal yang diinginkan
        DateFormat formatTanggal = new SimpleDateFormat("dd/MM/yyyy");

        // Mengonversi tanggal menjadi string sesuai format
        String tanggalString = formatTanggal.format(tanggalSekarang);

        return tanggalString;
    }

    public String getReferensi() {

        String karakter = "abcdefghijklmnopqrstuvwxyz1234567890";
        int panjang = 20;
        Random random = new Random();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < panjang; i++) {
            int index = random.nextInt(karakter.length());
            char randomChar = karakter.charAt(index);
            sb.append(randomChar);
        }

        return sb.toString();
    }

    public int getkodeTransaksi() {
        int kodetransaksi;
        Random kode = new Random();
        kodetransaksi = kode.nextInt(6);
        return kodetransaksi;
    }

    public String getWaktu() {
        // Mendapatkan waktu sekarang
        LocalTime waktuSekarang = LocalTime.now();

        // Mengatur format waktu yang diinginkan
        DateTimeFormatter formatWaktu = DateTimeFormatter.ofPattern("HH:mm:ss");

        // Mengonversi waktu menjadi string sesuai format
        String waktuString = waktuSekarang.format(formatWaktu);

        return waktuString;
    }

    public int getJumlahTransaksi() {
        return jumlah_transaksi;
    }

    public String getNamaPengirim() {
        return user.getName();
    }

    public String getNoPengirim() {
        return user.getPhoneNumber();
    }

    public void setNamaPenerima(String nama) {
        this.namaTujuan = nama;
    }

    public void setNomorPenerima(String nomor) {
        this.noTujuan = nomor;
    }

    public String getNamaPenerima() {
        return namaTujuan;
    }

    public String getNoPenerima() {
        return noTujuan;
    }

    public void setJenisTransaksi(String jenis) {
        this.jenisTransaksi = jenis;
    }

    public String getJenisTrnsaksi() {
        return jenisTransaksi;
    }

    public abstract void cetakStruk();

}

class Transfer extends Transaksi {

    public Transfer(User user) {
        super();
        this.user = user;
        this.biaya_admin = 0;
    }

    public int setgetBiayaAdmin(int biaya_admin) {
        this.biaya_admin = biaya_admin;
        return biaya_admin;
    }

    public int setgetjumlah_transaksi(int jumlah_transaksi) {
        this.jumlah_transaksi = jumlah_transaksi;
        return jumlah_transaksi;
    }

    public int updateSaldo(int totalTransaksi) {
        user.setBalance(user.getBalance() - totalTransaksi);
        return user.getBalance();
    }

    public String getStatusBayar(boolean status_bayar) {
        if (status_bayar) {
            this.status_bayar = "Berhasil";
        }
        return this.status_bayar;
    }

    public void cetakStruk() {
        System.out.println("==================================================\n");
        System.out.println("                       OVO");
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Status Transaksi\t: " + status_bayar);
        System.out.println(" No. Referensi\t\t: " + getReferensi());
        System.out.println("\n Tanggal Transaksi\t: " + getTanggal());
        System.out.println(" Waktu Transaksi\t: " + getWaktu());
        System.out.println("\n--------------------------------------------------");
        System.out.println("\n Dari\t\t\t: " + getNamaPengirim());
        System.out.println(" OVO\t\t\t: " + getNoPengirim());
        System.out.println("\n Penerima\t\t: " + getNamaPenerima());
        System.out.println(" " + getJenisTrnsaksi() + "\t\t\t: " + getNoPenerima());
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Detail Pembayaran ");
        System.out.println("--------------------------------------------------");
        System.out.println("\n Nominal\t\t: " + setgetjumlah_transaksi(jumlah_transaksi));
        System.out.println(" Biaya\t\t\t: " + setgetBiayaAdmin(biaya_admin));
        System.out
                .println(" Total\t\t\t: " + (setgetBiayaAdmin(biaya_admin + setgetjumlah_transaksi(jumlah_transaksi))));
        System.out.println("\n--------------------------------------------------");

    }

}

class TopUp extends Transaksi {
    public TopUp(User user) {
        super();
        this.user = user;
    }

    public String setgetMetode(String metode) {
        this.metode = metode;
        return metode;
    }

    public String getNoTujuan() {
        return user.getPhoneNumber();
    }

    public int setgetJumlahTransaksi(int jumlah_transaksi) {
        this.jumlah_transaksi = jumlah_transaksi;
        return jumlah_transaksi;
    }

    public int setgetBiayaAdmin(int biaya_admin) {
        this.biaya_admin = biaya_admin;
        return biaya_admin;
    }

    public int updateSaldo(int totalTransaksi) {
        user.setBalance(user.getBalance() + totalTransaksi);
        return user.getBalance();
    }

    public void cetakMenu() {
        System.out.println("\n Pilih Metode :");
        System.out.println("\n==================================================");
        System.out.println(" 1. | Alfamart");
        System.out.println("--------------------------------------------------");
        System.out.println(" 2. | Indomaret");
        System.out.println("--------------------------------------------------");
        System.out.println(" 3. | ATM");
        System.out.println("--------------------------------------------------");
        System.out.println(" 4. | Internet / Mobile Banking");
        System.out.println("--------------------------------------------------");
        System.out.println(" 5. | Pengemudi Grab");
        System.out.println("--------------------------------------------------");
        System.out.println(" 6. | Tokopedia");
        System.out.println("--------------------------------------------------");
    }

    public void detailTransaksi(String metode, int pilihan, int totalTransaksi, int biaya_admin) {
        System.out.println("--------------------------------------------------");
        System.out.println(" Detail Transaksi ");
        System.out.println("--------------------------------------------------");
        System.out.println(" Metode Transaksi\t\t\t: " + metode);
        System.out.println(" Nominal Top Up\t\t\t\t: " + totalTransaksi);
        System.out.println(" Biaya Top Up\t\t\t\t: " + biaya_admin);
        System.out.println("--------------------------------------------------");
        System.out.println(" Total Pembayaran\t\t\t: " + (totalTransaksi + biaya_admin));
        this.jumlah_transaksi = totalTransaksi;
        updateSaldo(jumlah_transaksi);
    }

    public void menuBank() {
        System.out.println("==================================================");
        System.out.println(" Pilih Bank : ");
        System.out.println("--------------------------------------------------");
        System.out.println(" BCA");
        System.out.println("--------------------------------------------------");
        System.out.println(" MANDIRI");
        System.out.println("--------------------------------------------------");
        System.out.println(" BRI");
        System.out.println("--------------------------------------------------");
        System.out.println(" BNI");
        System.out.println("--------------------------------------------------");
        System.out.println(" BJB");
        System.out.println("--------------------------------------------------");
        System.out.println(" BJB SYARIAH");
        System.out.println("--------------------------------------------------");
        System.out.println(" BANK LAINNYA");
        System.out.println("--------------------------------------------------");
    }

    public void cetakStruk() {
        System.out.println("==================================================\n");
        System.out.println("                       OVO");
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Status Transaksi\t: " + status_bayar);
        System.out.println(" No. Referensi\t\t: " + getReferensi());
        System.out.println("\n Tanggal Transaksi\t: " + getTanggal());
        System.out.println(" Waktu Transaksi\t: " + getWaktu());
        System.out.println("\n--------------------------------------------------");
        System.out.println("\n Metode\t\t\t: " + setgetMetode(metode));
        System.out.println(" No. Tujuan\t\t: " + getNoTujuan());
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Detail Pembayaran ");
        System.out.println("--------------------------------------------------");
        System.out.println("\n Nominal\t\t: " + setgetJumlahTransaksi(jumlah_transaksi));
        System.out.println(" Biaya\t\t\t: " + setgetBiayaAdmin(biaya_admin));
        System.out
                .println(" Total\t\t\t: " + (setgetBiayaAdmin(biaya_admin + setgetJumlahTransaksi(jumlah_transaksi))));
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Klik ENTER untuk kembali ke menu utama : ");
    }
}

class tarikTunai extends Transaksi {
    public tarikTunai(User user) {
        super();
        this.user = user;
    }

    public int setgetBiayaAdmin(int biaya) {
        this.biaya_admin = biaya;
        return biaya_admin;
    }

    public int updateSaldo(int jumlah_transaksi, int biaya_admin) {
        user.setBalance(user.getBalance() - (jumlah_transaksi + biaya_admin));
        return user.getBalance();
    }

    public boolean getStatus(int jumlah_transaksi, int biaya_admin) {
        if ((user.getBalance() - (jumlah_transaksi + biaya_admin)) >= 0) {
            return true;
        }
        return false;
    }

    public void konfirmasi(String metode, int jumlah_transaksi, int biaya_admin) {
        char konfir;
        this.metode = metode;
        this.jumlah_transaksi = jumlah_transaksi;
        this.biaya_admin = biaya_admin;

        System.out.println("==================================================");
        System.out.println("\n KONFIRMASI TARIK TUNAI");
        System.out.println("\n--------------------------------------------------");
        System.out.println("\n No. HP\t\t: 20258-" + user.getPhoneNumber());
        System.out.println("\n Kode Transaksi\t: " + getkodeTransaksi());
        System.out.println("\n--------------------------------------------------");
        System.out.println("\n Metode Penarikan\t: " + metode);
        System.out.println("\n Jumlah Penarikan\t: " + jumlah_transaksi);
        System.out.println("\n Biaya Admin\t: " + biaya_admin);
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Total Pembayaran\t: " + (jumlah_transaksi + biaya_admin));
        System.out.println("--------------------------------------------------");
    }

    public void cetakStruk() {
        System.out.println(" ");
    }

}

class historiTransaksi {
    String namaTransaksi;
    String jenisTransaksi;
    String kodeTransaksi;
    String tanggal;
    String waktu;
    int totalTransaksi;

    public historiTransaksi(String namaTransaksi, String jenisTransaksi, String kodeTransaksi, String tanggal,
            String waktu, int totalTransaksi) {
        this.namaTransaksi = namaTransaksi;
        this.jenisTransaksi = jenisTransaksi;
        this.kodeTransaksi = kodeTransaksi;
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.totalTransaksi = totalTransaksi;
    }

    public void tampilHistori() {

    }
}

public class ovo {
    private ArrayList<historiTransaksi> histori = new ArrayList<>();
    private ArrayList<User> users = new ArrayList<>();
    private Scanner input = new Scanner(System.in);
    private String noHP; // Deklarasi variabel instance

    public static void main(String[] args) {
        ovo OVO = new ovo();
        OVO.run();
    }

    public void run() {
        users.add(new User("081280834772", "123", "Dewi Anggita", 10000000));
        users.add(new User("081395785212", "456", "Dinar Rizki", 5000000));
        System.out.println("==================================================\n");
        System.out.println("         SELAMAT DATANG DI APLIKASI OVO");
        System.out.println("\n--------------------------------------------------");
        System.out.println("\n MASUK ATAU DAFTAR");
        System.out.print(" Silahkan Masukan Nomor HP Anda : ");
        noHP = input.nextLine();

        if (isPhoneNumberRegistered(noHP)) {
            login();
        } else {
            daftar();
        }
    }

    public User getUserbyNoHP(String noHP) {
        for (User user : users) {
            if (user.getPhoneNumber().equals(noHP)) {
                return user;
            }
        }
        return null;
    }

    public boolean isPhoneNumberRegistered(String noHP) {
        for (User user : users) {
            if (user.getPhoneNumber().equals(noHP)) {
                return true;
            }
        }
        return false;
    }

    public void resetPassword() {
        clearScreen();
        System.out.println("==================================================\n");
        System.out.println("                 ATUR ULANG SANDI");
        System.out.println("\n--------------------------------------------------");
        User user = getUserbyNoHP(noHP);
        if (user != null) {
            System.out.print(" Masukkan sandi baru: ");
            String newPassword = input.nextLine();
            user.setPassword(newPassword);
            System.out.println(" Sandi berhasil diatur ulang.");
            login();
        } else {
            System.out.println(" Nomor HP tidak terdaftar.");
        }
    }

    public void login() {
        clearScreen();
        User user = getUserbyNoHP(noHP);
        System.out.println("==================================================\n");
        System.out.println("                      MASUK");
        System.out.println(" *nomor HP telah terdaftar, silahkan lakukan login");
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Nomor HP\t: " + noHP);
        System.out.print(" Input Sandi\t: ");
        String sandi = input.nextLine();

        if (user != null && user.getPassword().equals(sandi)) {
            clearScreen();
            System.out.println("\n Login Berhasil!");
            menuUtama();
        } else {
            System.out.println("--------------------------------------------------");
            System.out.println("\n Nomor HP atau Sandi salah");
            System.out.print(" Lupa Sandi? (y/n): ");
            String response = input.nextLine();
            if (response.equalsIgnoreCase("y")) {
                resetPassword();
            } else {
                login();
            }
        }
    }

    public void daftar() {
        clearScreen();
        System.out.println("==================================================\n");
        System.out.println("                     DAFTAR");
        System.out.println(" *nomor hp belum terdaftar, silahkan lakukan registrasi");
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Nomor HP\t\t: " + noHP);
        System.out.print(" Input Password\t\t: ");
        String sandi = input.nextLine();
        System.out.print(" Input Nama Pengguna\t: ");
        String nama = input.nextLine();
        int saldo = 0; // Deklarasi variabel saldo
        users.add(new User(noHP, sandi, nama, saldo));
        System.out.println("\n Daftar Berhasil!");
        login();
    }

    public void menuUtama() {
        clearScreen();
        User user = getUserbyNoHP(noHP);

        System.out.println("==================================================\n");
        System.out.println(" OVO CASH");
        System.out.println(" Total Saldo\t:\tRp. " + user.getBalance());
        System.out.println("\n==================================================");

        System.out.println("|    Top Up    |   Transfer   |    Tarik Tunai    |");
        System.out.println("|      (1)     |      (2)     |        (3)        |");
        System.out.println("--------------------------------------------------");
        System.out.println("|     Pulsa    |  Paket Data  |      History      |");
        System.out.println("|      (4)     |      (5)     |        (6)        |");
        System.out.println("==================================================");

        System.out.print("\n Input Menu\t: ");
        int pilihMenu = input.nextInt();

        switch (pilihMenu) {
            case 1:
                topup();
                break;
            case 2:
                transfer();
                break;
            case 3:
                tariktunai();
                break;

        }
    }

    public void topup() {
        int pilih;
        int jumlah_transaksi;
        int biaya_admin = 0;
        int minimalTopUp;
        String metode = "";
        String bankTujuan;
        User user = getUserbyNoHP(noHP);

        clearScreen();
        System.out.println("==================================================");
        System.out.println("\n                   Top Up");
        System.out.println("\n==================================================");
        up.cetakMenu();
        System.out.print(" Input metode pilihan\t: ");
        pilih = input.nextInt();
        input.nextLine();
        switch (pilih) {
            case 1:
                metode = "Alfamart";
                up.setgetMetode(metode);
                biaya_admin = 1500;
                up.setgetBiayaAdmin(biaya_admin);
                histori.add(new historiTransaksi(metode, ]================================))
                break;
            case 2:
                metode = "Indomaret";
                up.setgetMetode(metode);
                biaya_admin = 1500;
                up.setgetBiayaAdmin(biaya_admin);
                break;
            case 3:
                biaya_admin = 1500;
                up.setgetBiayaAdmin(biaya_admin);
                clearScreen();
                up.menuBank();
                System.out.println(" Input Bank Tujuan\t: ");
                bankTujuan = input.nextLine();
                if (bankTujuan.equalsIgnoreCase("bank lainnya")) {
                    System.out.print(" Input Nama Bank Lainnya\t: ");
                    metode = input.nextLine();
                    up.setgetMetode(metode);
                }
                break;
            case 4:
                biaya_admin = 1000;
                up.setgetBiayaAdmin(biaya_admin);
                clearScreen();
                up.menuBank();
                System.out.println(" Input Bank Tujuan\t: ");
                bankTujuan = input.nextLine();
                if (bankTujuan.equalsIgnoreCase("bank lainnya")) {
                    System.out.print(" Input Nama Bank Lainnya\t: ");
                    metode = input.nextLine();
                    up.setgetMetode(metode);
                    biaya_admin = 1500;
                }
                break;
            case 5:
                biaya_admin = 0;
                up.setgetBiayaAdmin(biaya_admin);
                metode = "Pengemudi Grab";
                up.setgetMetode(metode);
                break;
            case 6:
                biaya_admin = 1000;
                up.setgetBiayaAdmin(biaya_admin);
                metode = "Tokopedia";
                up.setgetMetode(metode);
                break;

        }

        clearScreen();
        System.out.println("--------------------------------------------------");
        System.out.println(" " + up.setgetMetode(metode));
        System.out.println("--------------------------------------------------");
        System.out.print(" Input Jumlah Top Up\t: ");
        jumlah_transaksi = input.nextInt();
        up.setgetJumlahTransaksi(jumlah_transaksi);
        input.nextLine();

        clearScreen();
        up.detailTransaksi(metode, pilih, jumlah_transaksi, biaya_admin);
        System.out.print(" Klik Y/y untuk mengkonfirmasi transaksi : ");
        input.nextLine();
        up.cetakStruk();
        input.nextLine();
        menuUtama();

    }

    public void transfer() {
        clearScreen();
        User user = getUserbyNoHP(noHP);
        Transfer transfer = new Transfer(user);
        boolean status_bayar = false;
        int tujuan;
        int minimal_transfer = 10000;
        int jumlahTransfer;
        int biayaAdmin;
        int totalTransaksi;
        String namaBank;
        String noRek;

        System.out.println("==================================================");
        System.out.println("\n                  Transfer");
        System.out.println("\n==================================================");
        System.out.println(" 1. | Ke Sesama OVO");
        System.out.println("--------------------------------------------------");
        System.out.println(" 2. | Ke Rekening Bank");
        System.out.println("--------------------------------------------------");
        System.out.print(" Input Pilihan\t: ");
        tujuan = input.nextInt();
        input.nextLine();

        switch (tujuan) {
            case 1:
                clearScreen();
                String jenisTransaksi = "OVO";
                String atasNama;
                String nomorHP;
                String namaTujuan;
                biayaAdmin = 0;

                System.out.println("==================================================");
                System.out.println("\n            Transfer ke Sesama OVO");
                System.out.println("\n             Minimal Transfer = " + minimal_transfer);
                System.out.println("\n==================================================");
                System.out.print(" \n Masukkan nomor HP penerima\t: ");
                nomorHP = input.nextLine();
                transfer.setNomorPenerima(nomorHP);
                System.out.print(" \n Masukkan nama penerima\t\t: ");
                namaTujuan = input.nextLine();
                transfer.setNamaPenerima(namaTujuan);
                transfer.setJenisTransaksi(jenisTransaksi);

                do {
                    System.out
                            .print(" \n Masukkan jumlah transfer\t: ");
                    jumlahTransfer = input.nextInt();
                    transfer.setgetjumlah_transaksi(jumlahTransfer);
                    input.nextLine();

                    if (jumlahTransfer < minimal_transfer) {
                        System.out.println("\n Nominal Transfer kurang dari Nilai Minimal");
                        System.out.println("\n--------------------------------------------------");

                    }
                } while (jumlahTransfer < minimal_transfer);

                totalTransaksi = jumlahTransfer + biayaAdmin;

                if (totalTransaksi <= user.getBalance()) {
                    transfer.updateSaldo(totalTransaksi);
                    status_bayar = true;
                    transfer.getStatusBayar(status_bayar);
                    clearScreen();
                    transfer.cetakStruk();
                } else {
                    System.out.println("\n Saldo Anda tidak cukup");
                    menuUtama();
                }
                break;

            case 2:
                clearScreen();
                biayaAdmin = 2500;
                transfer.setgetBiayaAdmin(biayaAdmin);
                System.out.println("==================================================");
                System.out.println("\n            Transfer ke Rekening Bank");
                System.out.println("\n             Minimal Transfer = " + minimal_transfer);
                System.out.println("\n==================================================");
                System.out.println(" BANK TUJUAN :");
                System.out.println(" BCA\t\tBRI\t\tMANDIRI");
                System.out.println(" CIMB NIAGA\tBNI\t\tBTPN");
                System.out.print(" \n Pilih Bank Tujuan\t: ");
                namaBank = input.nextLine();
                transfer.setJenisTransaksi(namaBank.toUpperCase());
                System.out.print(" Input Nama Penerima\t: ");
                atasNama = input.nextLine();
                transfer.setNamaPenerima(atasNama);
                System.out.print(" Input Nomor Rekening\t: ");
                noRek = input.nextLine();
                transfer.setNomorPenerima(noRek);

                do {
                    System.out
                            .print(" \n Masukkan jumlah transfer\t: ");
                    jumlahTransfer = input.nextInt();
                    transfer.setgetjumlah_transaksi(jumlahTransfer);
                    input.nextLine();

                    if (jumlahTransfer < minimal_transfer) {
                        System.out.println("\n Nominal Transfer kurang dari Nilai Minimal");
                        System.out.println("\n--------------------------------------------------");

                    }
                } while (jumlahTransfer < minimal_transfer);

                totalTransaksi = jumlahTransfer + biayaAdmin;

                if (totalTransaksi <= user.getBalance()) {
                    transfer.updateSaldo(totalTransaksi);
                    status_bayar = true;
                    transfer.getStatusBayar(status_bayar);
                    clearScreen();
                    transfer.cetakStruk();
                } else {
                    System.out.println("\n Saldo Anda tidak cukup");
                    menuUtama();
                }
                break;
        }
        System.out.print("Klik ENTER untuk kembali ke menu utama ");
        input.nextLine();
        menuUtama();
    }

    public void tariktunai() {
        clearScreen();
        String metode;
        String klik;
        User user = getUserbyNoHP(noHP);
        tarikTunai tarik = new tarikTunai(user);
        int pilih;
        int minimal = 50000;
        int jumlah_transaksi;
        int biaya_admin = 5000;
        tarik.setgetBiayaAdmin(biaya_admin);

        System.out.println("==================================================");
        System.out.println("                  Tarik Tunai");
        System.out.println("==================================================");
        System.out.println("\n 1.  Tarik Tunai melalui BANK BCA");
        System.out.println("\n 2.  Tarik Tunai melalui Indomaret");
        System.out.println("\n--------------------------------------------------");
        System.out.println(" Input metode tarik tunai\t: ");
        pilih = input.nextInt();
        input.nextLine();

        switch (pilih) {
            case 1:
                clearScreen();
                metode = "BANK BCA";
                System.out.println("==================================================");
                System.out.println(" Tarik Tunai melalui " + metode);
                System.out.println(" Minimal transaksi Rp 50.000");
                System.out.println("==================================================");
                System.out.print(" Input Jumlah Transaksi\t: ");
                jumlah_transaksi = input.nextInt();
                input.nextLine();
                clearScreen();
                tarik.konfirmasi(metode, jumlah_transaksi, biaya_admin);
                tarik.updateSaldo(jumlah_transaksi, biaya_admin);
                System.out.println(" Klik Y/y untuk mengkonfirmasi dan menyelesaikan Transaksi : ");
                klik = input.nextLine();
                menuUtama();

                break;
            case 2:
                clearScreen();
                metode = "Indomaret";
                metode = "BANK BCA";
                System.out.println("==================================================");
                System.out.println(" Tarik Tunai melalui " + metode);
                System.out.println(" Minimal transaksi Rp 50.000");
                System.out.println("==================================================");
                System.out.print(" Input Jumlah Transaksi\t: ");
                jumlah_transaksi = input.nextInt();
                input.nextLine();
                clearScreen();
                tarik.konfirmasi(metode, jumlah_transaksi, biaya_admin);
                tarik.updateSaldo(jumlah_transaksi, biaya_admin);
                System.out.println(" Klik Y/y untuk mengkonfirmasi dan menyelesaikan Transaksi : ");
                klik = input.nextLine();
                menuUtama();

                break;
        }

    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
